from django.test import Client
from django.test import TestCase, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep

# Create your tests here.
class AppTest(TestCase):
    # Test for index page
    def test_index_response(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'index.html')

    # Test for login page
    def test_login_page_response(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code,200)
    
    def test_login_page_template(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response,'login.html')

    def test_login_username_found(self):
        data = {'username':'dummy','password1':'dummyuser123'}
        data['password2'] = data['password1']
        data['email'] = 'sealspinning@sealspinning.com'
        data['first_name'] = 'sealspinning'
        data['last_name'] = 'happy'
        data['photo_url'] = 'https://scele.cs.ui.ac.id/pluginfile.php/1/theme_lambda/logo/1588739241/5.png'

        response = self.client.post('/register',data)
        response = self.client.post('/login',{'username':'dummy','password':'dummyuser123'})
        content = response.content.decode('utf8')
        self.assertFalse('dummyuser123' in content)

    def test_login_username_notfound(self):
        response = self.client.post('/login',{'username':'aku','password':'suka'})
        content = response.content.decode('utf8')
        self.assertTrue('Login' in content)

    # Test for register page
    def test_register_page_response(self):
        response = Client().get('/register')
        self.assertEqual(response.status_code,200)
    
    def test_register_page_template(self):
        response = Client().get('/register')
        self.assertTemplateUsed(response,'register.html')

    def test_register_submit(self):
        data = {'username':'dummy','password1':'dummyuser123'}
        data['password2'] = data['password1']
        data['email'] = 'sealspinning@sealspinning.com'
        data['first_name'] = 'sealspinning'
        data['last_name'] = 'happy'
        data['photo_url'] = 'https://scele.cs.ui.ac.id/pluginfile.php/1/theme_lambda/logo/1588739241/5.png'

        response = self.client.post('/register',data)
        content = response.content.decode('utf8')
        self.assertFalse('Login' in content)

    # Test for register page
    def test_logout_page_response(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code,302)

class FunctionalTest(LiveServerTestCase):
    
    def setUp(self):
        super().setUp()
        self.link = self.live_server_url# "http://fbunaren.herokuapp.com" #
        opt = webdriver.ChromeOptions()
        opt.add_argument('--no-sandbox')
        opt.add_argument('--headless')
        opt.add_argument('--disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(chrome_options=opt,executable_path='chromedriver')

    def tearDown(self):
        self.selenium.refresh()
        self.selenium.quit()
        super().tearDown()

    def test_index(self):
        self.selenium.get(self.link)
        #Test if the Webpage is Loaded
        #Test h1 Header
        self.assertTrue(self.selenium.find_elements_by_tag_name('h1') != None)

    def test_register(self):
        try:
            self.selenium.get(self.link + "/register")
            #Test if the Webpage is Loaded
            #Test h1 Header
            self.assertTrue(self.selenium.find_elements_by_tag_name('h1') != None)
        except:
            pass
    
    def test_login(self):
        try:
            self.selenium.get(self.link + "/login")
            #Test if the Webpage is Loaded
            #Test h1 Header
            self.assertTrue(self.selenium.find_elements_by_tag_name('h1') != None)
        except:
            pass