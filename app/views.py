from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth.models import User
from django import forms

# Extended Form
'''
class UserCreateForm(UserCreationForm):
    first_name = forms.CharField(required=True,max_length=100)
    last_name = forms.CharField(required=True,max_length=100)
    email = forms.EmailField(required=True)
    photo_url = forms.CharField(required=True,max_length=1024)

    class Meta:
        model = User
        fields = ("username", "email",'first_name','last_name', "password1", "password2",'photo_url')

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user
'''

# Create your views here.
def index(request):
    return render(request,'index.html')

def login_page(request):
    form = AuthenticationForm()
    response = {'form':form}
    if request.method == 'POST':
        username = request.POST['username']
        password =  request.POST['password']
        post = User.objects.filter(username=username)
        if post:
            username = request.POST['username']
            raw_password = request.POST['password']
            user = authenticate(username=username, password=raw_password)
            request.session['username'] = username
            login(request, user)
            return redirect("index")
    return render(request,'login.html',response)

def register_page(request):
    form = UserCreationForm()
    response = {'form':form}
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            #form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            # ,photo_url=request.POST['photo_url']
            User.objects.create_user(username,request.POST['email'],raw_password,first_name=request.POST['first_name'],last_name=request.POST['last_name'])
            user = authenticate(username=username, password=raw_password)
            request.session['username'] = username
            login(request, user)
            return redirect('index')
    return render(request,'register.html',response)

def logout(request):
    try:
        del request.session['username']
    except:
        pass
    return redirect("/accounts/logout/?next=/")