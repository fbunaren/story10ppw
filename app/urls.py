from django.urls import path
from django.conf.urls import include
from .views import *
from django.conf import settings
from . import views

urlpatterns = [
    path('', index, name='index'),
    path('login', login_page, name='login'),
    path('register', register_page, name='register'),
    path('logout', logout, name='logout'),
    ]